package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InputOutput {

    public static void imprimirMensagemIncial(){
        System.out.println("Bem vindo ao sistema");
    }
    public static Map<String , String > solicitarDados(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite a quantidade de jogadas :");
        Integer quantidade = scanner.nextInt();

        Map<String , String > dados = new HashMap<>();
        dados.put("quantidade", quantidade.toString());

        return dados;
    }
}
