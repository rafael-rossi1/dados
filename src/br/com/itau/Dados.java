package br.com.itau;

import java.util.Random;

public class Dados {
    private int quantidade;
    private int soma;

    public Dados(int quantidade) {
        this.quantidade = quantidade;
        this.soma = soma;
    }


    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getSoma() {
        return soma;
    }

    public void setSoma(int soma) {
        this.soma = soma;
    }
}

